clear all;
clc;


theta1 = zeros(191,1); 

t = 0;
for Y = 1.1:0.01:3;
t = t+1;
% the function about theta1
f  = @(x) exp(psi(x))*Y-x;
% the derivative of the above function
f1 = @(x) exp(psi(x))*psi(1,x)*Y-1;
% initial value
theta1_old = 3;
% Newton's method to find the root of f
    for n = 1:10000
        theta1_new = theta1_old-f(theta1_old)/f1(theta1_old);
        if abs(f(theta1_new))<1e-8;
           theta1(t) = theta1_new;
           break
        else
           theta1_old = theta1_new;
        end
    end 
end

clf;

plot(1.1:0.01:3,theta1)
xlabel('Y_1/Y_2')
ylabel('\theta_1')