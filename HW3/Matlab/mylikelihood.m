function [f,f1] = mylikelihood(X,y,x)

f = sum(exp(X*[x(1),x(2),x(3),x(4),x(5),x(6)]')-X*[x(1),x(2),x(3),x(4),x(5),x(6)]'.*y+log(factorial(y)));

if nargout > 1
    f1 = (sum((exp(X*[x(1),x(2),x(3),x(4),x(5),x(6)]')-y)*ones(1,6).*X))';
end