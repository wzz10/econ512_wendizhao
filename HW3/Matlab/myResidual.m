function S = myResidual(X,y,x)

S = sum((y-exp(X*[x(1),x(2),x(3),x(4),x(5),x(6)]')).^2);

end