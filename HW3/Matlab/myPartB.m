clear all;
clc;

load('hw3.mat')

initial = [0,0,0,0,0,0]';

f  = @(beta) mylikelihood(X,y,beta); % likelihood function
fs = @(beta) myResidual(X,y,beta);   % residual sum of squares

% FMINUNC without a derivative supplied
options = optimoptions(@fminunc,'FunctionTolerance',eps,'Display','off','Algorithm','quasi-newton');
[a,fa] = fminunc(f,initial,options);

% FMINUNC with a derivative supplied
options = optimoptions(@fminunc,'FunctionTolerance',eps,'Display','off','Algorithm','quasi-newton','SpecifyObjectiveGradient',true);
b = fminunc(f,initial,options);

% Nelder Mead method
c = fminsearch(f,initial,optimset('TolX',1e-8,'MaxFunEvals',1e6));

% BHHH method
[d,~,J,H] = myBHHH(X,y,initial);

% NLLS
options = optimoptions(@fminunc,'FunctionTolerance',eps,'Display','off','Algorithm','quasi-newton');
e = fminunc(fs,initial,options);

% Hessian and eigenvalue for the initial value of beta
H0 = -(ones(length(y),6).*X)'*(ones(length(y),6).*X)./length(y);
eig0 = eig(H0);
% Eigenvalue for the Hessian at the estimated parameters
eig  = eig(H);

% Covariance matrix and stand error for MLE
MLEcov = H\J/H;
MLEstd = sqrt(diag(MLEcov));

% Covariance matrix and stand error for NLLS
NLLScov = myNLLScov(X,y,e);
NLLSstd = sqrt(diag(NLLScov));

% ----------------------------- Print -----------------------------
diary('Results HW3');
disp('----------------------------- Part B -----------------------------');
disp(' ')
disp('--------------------- Question 1 ---------------------');
disp('FMINUNC without a derivative supplied');
disp(['Beta = ', num2str(a')]);
disp(' ')
disp('FMINUNC with a derivative supplied');
disp(['Beta = ', num2str(b')]);
disp(' ')
disp('Nelder Mead method');
disp(['Beta = ', num2str(c')]);
disp(' ')
disp('BHHH method');
disp(['Beta = ', num2str(d')]);
disp(' ')
disp('Function evaluation is');
disp(['lnL = ', num2str(-fa')]);
disp(' ')
disp('--------------------- Question 2 ---------------------');
disp('The eigenvalues for the initial Hessian is');
disp(eig0');
disp(' ')
disp('The eigenvalues for the Hessian at the estimated parameters is');
disp(eig');
disp(' ')
disp('--------------------- Question 3 ---------------------');
disp('The NLLS estimators are');
disp(['Beta = ', num2str(e')]);
disp(' ')
disp('--------------------- Question 4 ---------------------');
disp('The standard errors for the BHHH are');
disp(MLEstd');
disp(' ')
disp('The standard errors for the NLLS are');
disp(NLLSstd');
diary off