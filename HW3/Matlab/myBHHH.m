% INPUTS
%   (X,y)   : data
%   beta0   : initial value

% OUTPUTS
%   beta    : MLE
%   n       : number of iteration
%   J       : J
%   H       : Hessian

function [beta,n,J,H] = myBHHH(X,y,beta0)

beta_old = beta0;

for n=1:1000
    J = ((y-exp(X*beta_old))*ones(1,6).*X)'*((y-exp(X*beta_old))*ones(1,6).*X);
    G = (sum((y-exp(X*beta_old))*ones(1,6).*X))';
    beta_new = beta_old+J\G;
    if abs(G) < 1e-8
        beta = beta_new;
        break
    else
        beta_old = beta_new;
    end
end

J = ((y-exp(X*beta))*ones(1,6).*X)'*((y-exp(X*beta))*ones(1,6).*X)./length(y);
H = -(exp(X*beta)*ones(1,6).*X)'*(exp(X*beta)*ones(1,6).*X)./length(y);