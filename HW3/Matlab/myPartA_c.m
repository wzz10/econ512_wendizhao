% INPUTS
%   x       : gamma distributed data
%   initial : initial value of theta1 in the Newton's method

% OUTPUTS
%   theta   : MLE of theta
%   cov1    : covaniance matrix by useing -H^(-1)
%   cov2    : covaniance matrix by useing J^(-1)
%   cov3    : covaniance matrix by useing H^(-1)*J*H^(-1)

function [theta,cov1,cov2,cov3] = myPartA_c(x,initial)

Y1 = mean(x);
Y2 = exp(mean(log(x)));

% the function about theta1
f  = @(x) exp(psi(x))*Y1-x*Y2;
% the derivative of the above function
f1 = @(x) exp(psi(x))*psi(1,x)*Y1-Y2;
% initial value
theta1_old = initial;
% Newton's method to find the root of f
for n = 1:10000
    theta1_new = theta1_old-f(theta1_old)/f1(theta1_old);
    if abs(f(theta1_new))<1e-8;
       theta1 = theta1_new;
       break
    else
       theta1_old = theta1_new;
    end
end 
% find theta2
theta2 = theta1/Y1;
theta = [theta1; theta2];

% calculate the covariance matrix
H = [-psi(1,theta1),1/theta2;1/theta2,-theta1/theta2^2];

J = [mean((log(theta2)+log(x)-psi(theta1)).^2),mean((log(theta2)+log(x)-psi(theta1)).*(Y1-x));...
    mean((log(theta2)+log(x)-psi(theta1)).*(Y1-x)),mean((Y1-x).^2)];

cov1 = -inv(H);
cov2 = inv(J);
cov3 = H\J/H;