function cov = myNLLScov(X,y,beta)

A = (exp(X*beta)*ones(1,6).*X)'*(exp(X*beta)*ones(1,6).*X)./length(y);
B = ((y-exp(X*beta)).*exp(X*beta)*ones(1,6).*X)'*((y-exp(X*beta)).*exp(X*beta)*ones(1,6).*X)./length(y);
cov = A\B/A;

end