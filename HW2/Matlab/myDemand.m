% The demand function
function [Q] = myDemand(P)
de = 1+exp(-1-P(1))+exp(-1-P(2))+exp(-1-P(3));
Q = exp(-1-P)/de;
end