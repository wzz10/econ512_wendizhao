% please use section separator double percent sign
clear all
clc
warning('off','all')
addpath('../CEtools/');

diary('HW1_Result')

P_ini1 = [1, 1, 1]';
P_ini2 = [0, 0, 0]';
P_ini3 = [0, 1, 2]';
P_ini4 = [3, 2, 1]';

%% ----------------------------- Question 2 -----------------------------
disp('----------------------------- Question 2 -----------------------------');
optset('broyden','showiters',100);
optset('broyden','maxit',100) ;
optset('broyden','tol',1e-8) ;

tic
eqm1 = broyden(@(P) myEqm(P),P_ini1)
toc

tic
eqm2 = broyden(@(P) myEqm(P),P_ini2)
toc

tic
eqm3 = broyden(@(P) myEqm(P),P_ini3)
toc

tic
eqm4 = broyden(@(P) myEqm(P),P_ini4)
toc

%% ----------------------------- Question 3 -----------------------------
disp('----------------------------- Question 3 -----------------------------');

tic
[eqm1,it1] = myQ3(@(P) myEqm(P),P_ini1+1,P_ini1)
toc

tic
[eqm2,it2] = myQ3(@(P) myEqm(P),P_ini2+1,P_ini2)
toc

tic
[eqm3,it3] = myQ3(@(P) myEqm(P),P_ini3+1,P_ini3)
toc

tic
[eqm4,it4] = myQ3(@(P) myEqm(P),P_ini4+1,P_ini4)
toc

%% ----------------------------- Question 4 -----------------------------
disp('----------------------------- Question 4 -----------------------------');

tic
[eqm1,it1] = myQ4(P_ini1)
toc

tic
[eqm2,it2] = myQ4(P_ini2)
toc

tic
[eqm3,it3] = myQ4(P_ini3)
toc

tic
[eqm4,it4] = myQ4(P_ini4)
toc

%% ----------------------------- Question 5 -----------------------------
disp('----------------------------- Question 5 -----------------------------');


tic
[eqm1,it1] = myQ5(@(P) myEqm(P),P_ini1+1,P_ini1)
toc

tic
[eqm2,it2] = myQ5(@(P) myEqm(P),P_ini2+1,P_ini2)
toc

tic
[eqm3,it3] = myQ5(@(P) myEqm(P),P_ini3+1,P_ini3)
toc

tic
[eqm4,it4] = myQ5(@(P) myEqm(P),P_ini4+1,P_ini4)
toc
 
diary off