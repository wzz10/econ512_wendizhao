% The equilibrium conditions
function [fval] = myEqm(P)
fval = P.*(1-myDemand(P))-1;
end