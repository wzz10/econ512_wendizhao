% INPUTS
%   f       : name of function of form:
%   pold    : initial guess for root
%   pnew    : second guess for root

% OUTPUTS
%   p       : root of f

function [p] = myNewtonsecant(f,pnew,pold)

maxit = 50;

for it = 1:maxit
    
    p = pnew-f(pnew)*(pnew-pold)/(f(pnew)-f(pold));
    
    if abs(f(pnew)) < 1e-3
        break
    end
    
    pold = pnew;
    pnew = p;
    
end

end