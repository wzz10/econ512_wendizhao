function [Pnew,it] = myQ4(P)

maxit = 500;
Pnew = P;

for it = 1:maxit
    
    Q = myDemand(Pnew);
    Pnew = 1./(1-Q);
    
    if max(abs(myEqm(Pnew))) < 1e-8
        break
    end
end

end