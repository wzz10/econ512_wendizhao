% INPUTS
%   f       : name of function of form:
%   Pold    : initial guess for root
%   Pnew    : second guess for root

% OUTPUTS
%   P       : root of f
%   it      : number of iterations performed

function [P,it] = myQ3(f,Pnew,Pold)

maxit = 500;
P = zeros(3,1);

for it = 1:maxit
    
    P(1) = myNewtonsecant(@(p1) f([p1,Pold(2),Pold(3)]),Pnew(1),Pold(1));
    P(2) = myNewtonsecant(@(p2) f([Pold(1),p2,Pold(3)]),Pnew(2),Pold(2));      
    P(3) = myNewtonsecant(@(p3) f([Pold(1),Pold(2),p3]),Pnew(3),Pold(3));   
    if max(abs(f(Pnew))) < 1e-8
        break
    end
    
    Pold = Pnew;
    Pnew = P;
    
end

end