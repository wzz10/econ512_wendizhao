% INPUTS
%   f       : name of function of form:
%   Pold    : initial guess for root
%   Pnew    : second guess for root

% OUTPUTS
%   P       : root of f
%   it      : number of iterations performed

function [P,it] = myQ5(f,Pnew,Pold)

maxit = 500;

for it = 1:maxit
    
    Pnew1 = [Pnew(1), Pold(2), Pold(3)]';
    Pnew2 = [Pold(1), Pnew(2), Pold(3)]';
    Pnew3 = [Pold(1), Pold(2), Pnew(3)]';
    
    coef1 = (Pnew1-Pold)./(f(Pnew1)-f(Pold));
    coef2 = (Pnew2-Pold)./(f(Pnew2)-f(Pold));
    coef3 = (Pnew3-Pold)./(f(Pnew3)-f(Pold));
    coef  = [coef1(1), coef2(2), coef3(3)]';
    
    P = Pnew-f(Pnew).*coef;

    if max(abs(f(Pnew))) < 1e-8
        break
    end
    
    Pold = Pnew;
    Pnew = P;
    
end
end