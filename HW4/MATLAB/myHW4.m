clear all
clc

%% -------------------------------- Part 1 --------------------------------
x0 = [0.1,1,0];
GQ = @(x) myGQ(x(1),x(2),x(3));
L1  = -GQ(x0);
%  this is incorrect value. I think it is due your incorrect calculation of
%  loglikelihood. 
%% -------------------------------- Part 2 --------------------------------
MC = @(x) myMC(x(1),x(2),x(3));
L2 = -MC(x0);

%% -------------------------------- Part 3 --------------------------------
% Constraint that variance is non-negative
A  = [0,-1,0];
b  = 0;

[x1,fval1] = fmincon(GQ,x0,A,b);
[x2,fval2] = fmincon(MC,x0,A,b);
% these values are worg since you have wrong calculations of likelihood.
% everithing after this point is wrong. 
%% -------------------------------- Part 4 --------------------------------
x0 = [0.1,0.1,1,1,0.5,0];
% Constraints that variance is non-negative and correlation coefficient is
% greater than -1 and less than 1
A  = [0,0,-1,0,0,0;...
      0,0,0,-1,0,0;...
      0,0,0,0,1,0;...
      0,0,0,0,-1,0];
b  = [0;0;1;1];

MC2 = @(x) myMC2([x(1),x(2),x(3),x(4),x(5),x(6)]);
[x3,fval3] = fmincon(MC2,x0,A,b);

%% -------------------------------- Print --------------------------------
diary('Results HW4');
disp('--------------------- Part 1 ---------------------');
disp('The value of likelihood function by Gauss Quadrature is');
disp(['L = ', num2str(exp(L1)),'*1e-600']);
disp(' ')
disp('--------------------- Part 2 ---------------------');
disp('The value of likelihood function by Monte Carlo Methods is');
disp(['L = ', num2str(exp(L2)),'*1e-600']);
disp(' ')
disp('--------------------- Part 3 ---------------------');
disp('The MLE by Gauss Quadrature is');
disp(['Beta_0     = ', num2str(x1(1))]);
disp(['Sigma_Beta = ', num2str(x1(2)^2)]);
disp(['Gamma      = ', num2str(x1(3))]);
disp('The value of the likelihood function is');
disp(['L = ', num2str(exp(-fval1)),'*1e-600']);
disp(' ')
disp('The MLE by Monte Carlo Methods is');
disp(['Beta_0     = ', num2str(x2(1))]);
disp(['Sigma_Beta = ', num2str(x2(2)^2)]);
disp(['Gamma      = ', num2str(x2(3))]);
disp('The value of the likelihood function is');
disp(['L = ', num2str(exp(-fval2)),'*1e-600']);
disp(' ')
disp('--------------------- Part 4 ---------------------');
disp('The MLE by Monte Carlo Methods is');
disp(['Beta_0       = ', num2str(x3(1))]);
disp(['u_0          = ', num2str(x3(2)^2)]);
disp(['Sigma_Beta   = ', num2str(x3(3)^2)]);
disp(['Sigma_u      = ', num2str(x3(2)*x3(3)*x3(4))]);
disp(['Sigma_Beta_u = ', num2str(x3(2))]);
disp(['Gamma        = ', num2str(x3(6))]);
disp('The value of the likelihood function is');
disp(['L = ', num2str(exp(-fval3)),'*1e-600']);
diary off