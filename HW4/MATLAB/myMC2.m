function L = myMC2(x)


load('hw4data')
% loading data in function makes that function ridiculously slow
rng(30);
mu = [x(1);x(2)];
sigma = [x(3)^2,x(3)*x(4)*x(5);x(3)*x(4)*x(5),x(4)^2];
gamma = x(6);

L = 1;
for i = 1:data.N
    Li = ones(100,1);
    r = mvnrnd(mu,sigma,100);
    beta = r(:,1);
    u = r(:,2);
    for t = 1:data.T
    X = data.X(t,i);
    Y = data.Y(t,i)*ones(100,1);
    Z = data.X(t,i)*ones(100,1);
    Lit = (exp(X*beta+gamma*Z+u)./(1+exp(X*beta+gamma*Z+u))).^Y...
        .*(1./(1+exp(X*beta+gamma*Z+u))).^(1-Y);
    Li = Li.*Lit;
    end
    Li_int = mean(Li)*1e6;
    L = L*Li_int;
end
    L=-log(L);
end