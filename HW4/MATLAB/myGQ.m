function L = myGQ(mu,sigma,gamma)

load('hw4data')
% do not load data inside the function. makes it slow.
[x,w] = GaussHermite(20);
beta = sqrt(2)*sigma*x+mu;

L = 1;
for i = 1:data.N
    Li = ones(20,1);
    for t = 1:data.T
    X = data.X(t,i);
    Y = data.Y(t,i)*ones(20,1);
    Z = data.X(t,i)*ones(20,1);
    Lit = (exp(X*beta+gamma*Z)./(1+exp(X*beta+gamma*Z))).^Y...
        .*(1./(1+exp(X*beta+gamma*Z))).^(1-Y);
    Li = Li.*Lit;
    end
    Li_int = Li'*w*pi^(-1/2)*1e6;
%     why you use 1e6 multiplier? it may cause the error in the value .
    L = L*Li_int;
%     it is extremely bad practice to multiply likelihood and then take
%     log. it will kill the mantissa of your value since we have finite
%     precision computers. always do logarithm inside the loop and keep the
%     sum of logs.
end
    L=-log(L);
end