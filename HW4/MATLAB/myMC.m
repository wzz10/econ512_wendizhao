function L = myMC(mu,sigma,gamma)

load('hw4data')
rng(30);

L = 1;
for i = 1:data.N
    Li = ones(100,1);
    beta = randn(100,1)*sigma+mu;
    for t = 1:data.T
    X = data.X(t,i);
    Y = data.Y(t,i)*ones(100,1);
    Z = data.X(t,i)*ones(100,1);
    Lit = (exp(X*beta+gamma*Z)./(1+exp(X*beta+gamma*Z))).^Y...
        .*(1./(1+exp(X*beta+gamma*Z))).^(1-Y);
    Li = Li.*Lit;
    end
    Li_int = mean(Li)*1e6;
    L = L*Li_int;
end
    L=-log(L);
end